// Hotel Database

// [Insert a single room] (insertOne Method)

db.rooms.insert({
	name: "single",
	accomodates: 2,
	price: 1000,
	description: "A simple room with all the basic necessities",
	rooms_available: 10,
	isAvailable: false
})

// [Insert a single room] (insertOne Method End)

// [Insert a multiple rooms] (insertMany Method)

db.rooms.insertMany([
	{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A room fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
	}
])

// [Insert a multiple rooms End] (insertMany Method End)

// (FIND Method)

db.rooms.find({ name: "double" })

// (FIND Method End)

// (updateOne Method)
db.rooms.updateOne(
	{
		name: "queen"
	},
	{
		$set: {
			rooms_available: 0
		}
	}
)

// (updateOne Method End) 

// (deleteMany Method)

db.rooms.deleteMany({
	rooms_available: 0
})
// (deleteMany Method End)